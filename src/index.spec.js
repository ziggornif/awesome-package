const { hello, sayHello } = require('./index');

describe('Hello', () => {
  test('should return hello world', () => {
    expect(hello()).toBe('Hello world 👋');
  });

  test('should return hello <caller>', () => {
    expect(hello('matthieu')).toBe('Hello matthieu 👋');
  });

  test('should say hello world', () => {
    const spy = jest.spyOn(console, 'log');
    sayHello();
    expect(spy).toHaveBeenCalledWith('Hello world 👋');
  });

  test('should say hello <caller>', () => {
    const spy = jest.spyOn(console, 'log');
    sayHello('matthieu');
    expect(spy).toHaveBeenCalledWith('Hello matthieu 👋');
  });
});
