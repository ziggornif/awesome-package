/**
 * Hi !
 * @param {string} name
 * @returns {string}
 */
function hello(name = 'world') {
  return `Hello ${name} 👋`;
}

/**
 * Say hello to caller
 * @param {string} name
 */
function sayHello(name) {
  console.log(hello(name));
}

module.exports = {
  hello,
  sayHello,
};
