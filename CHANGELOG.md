# [1.2.0](https://gitlab.com/ziggornif/awesome-package/compare/v1.1.0...v1.2.0) (2021-12-11)


### Features

* **ci:** add test stage ([657b6b7](https://gitlab.com/ziggornif/awesome-package/commit/657b6b7ec9ed99cb7b83ba41c2564bc27ab092c5))
* **hello:** add new function ([567a9ee](https://gitlab.com/ziggornif/awesome-package/commit/567a9eed2685393d5fbb9797f7d7d793e1769417))

# [1.1.0](https://gitlab.com/ziggornif/awesome-package/compare/v1.0.0...v1.1.0) (2021-12-11)


### Features

* **changelog:** try changelog config ([1c567c4](https://gitlab.com/ziggornif/awesome-package/commit/1c567c4d17482fe4a459585141b119fa2959de42))
